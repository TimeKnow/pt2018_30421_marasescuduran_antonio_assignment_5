package pt2018_30421_marasescuduran_antonio_assignment_5;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        List<MonitoredData> monitoredDataList = ServiceClass.readData("Activities.txt");
        long count = ServiceClass.countDistinctDays(monitoredDataList);
        HashMap<String,Integer> activitiesCount = ServiceClass.countActivity(monitoredDataList);
        HashMap<Integer,HashMap<String,Integer>> daysActivities = ServiceClass.dayActivities(monitoredDataList);
        HashMap<String,Date> activtiesDuration = ServiceClass.getActivitiesDuration(monitoredDataList);
        List<String> filteredActivities = ServiceClass.filterActivities(monitoredDataList);
        List<String> greater = ServiceClass.greater(monitoredDataList);
        try{
            BufferedWriter bw = new BufferedWriter(new FileWriter("Ex0"));
            for(MonitoredData a : monitoredDataList)
                bw.write(a.toString()+"\n");
            bw.close();

            bw = new BufferedWriter(new FileWriter("Ex1"));
            bw.write(String.valueOf(count));
            bw.close();
            bw = new BufferedWriter(new FileWriter("Ex2"));
            bw.write(activitiesCount.toString());
            bw.close();
            bw = new BufferedWriter(new FileWriter("Ex3"));
            for(Map.Entry<String,Date> a : activtiesDuration.entrySet())
                bw.write(a.getKey()+" "+a.getValue().getHours()+":"+a.getValue().getMinutes()+":"+a.getValue().getSeconds()+"\n");
            bw.close();
            bw = new BufferedWriter(new FileWriter("Ex4"));
            for(Map.Entry<Integer,HashMap<String,Integer>> a : daysActivities.entrySet()) {
                bw.write(a.getKey() + "\n");
                for(Map.Entry<String,Integer> b : a.getValue().entrySet())
                    bw.write("           "+b.getKey() + " "+b.getValue() + "\n");
                bw.write("\n\n\n");
            }
            bw.close();
            bw = new BufferedWriter(new FileWriter("Ex5"));
            bw.write(filteredActivities.toString());
            bw.close();
            bw = new BufferedWriter(new FileWriter("Ex6"));
            bw.write(greater.toString());
            bw.close();

        }catch (Exception e){
            return;
        }
    }
}
