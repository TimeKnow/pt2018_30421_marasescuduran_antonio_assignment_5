package pt2018_30421_marasescuduran_antonio_assignment_5;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MonitoredData{

    private String startTime;
    private String endTime;
    private String activities;

    public MonitoredData() {
    }

    public MonitoredData(String startTime, String endTime, String activities) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activities = activities;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getActivities() {
        return activities;
    }

    public void setActivities(String activities) {
        this.activities = activities;
    }

    public String getStartDay(){
        Matcher m = Pattern.compile("^(?<Date>\\d*-\\d*-\\d*)").matcher(this.startTime);
        m.find();
        return m.group("Date");
    }

    public String getEndDay(){
        Matcher m = Pattern.compile("^(?<Date>\\d*-\\d*-\\d*)").matcher(this.endTime);
        m.find();
        return m.group("Date");
    }

    public Date getDateStart(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        try{
            return format.parse(startTime);}
        catch (Exception e){
            return null;
        }
    }

    public Date getDateEnd(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        try{
            return format.parse(endTime);}
        catch (Exception e){return null;}
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MonitoredData)) return false;

        MonitoredData that = (MonitoredData) o;

        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) return false;
        return endTime != null ? endTime.equals(that.endTime) : that.endTime == null;
    }

    @Override
    public int hashCode() {
        int result = startTime != null ? startTime.hashCode() : 0;
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return
                "startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", activities='" + activities + '\'';
    }
}
