package pt2018_30421_marasescuduran_antonio_assignment_5;


import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class ServiceClass {

    public static List<String> getDistinctDays(List<MonitoredData> data){
        return Stream.concat(data.stream().map(x->x.getStartDay()).distinct(),data.stream().map(x->x.getEndDay()).distinct()).distinct().collect(Collectors.toList());
    }

    public static List<MonitoredData>  readData(String filename){
        List<MonitoredData> result = new ArrayList<>();
        try {
            result = Files.lines(Paths.get(filename)).map(res->{
                Matcher m = Pattern.compile("^(?<Data1>\\d*-\\d*-\\d* \\d*:\\d*:\\d*)\\s*(?<Data2>\\d*-\\d*-\\d* \\d*:\\d*:\\d*)\\s*(?<Data3>.*)$").matcher(res);
                m.find();
                return new MonitoredData(m.group("Data1"),m.group("Data2"),m.group("Data3"));
            }).collect(Collectors.toList());
        }catch (Exception e){
            return null;
        }
        for(MonitoredData d : result)
            System.out.println(d.toString());
        return result;
    }

    public static long countDistinctDays(List<MonitoredData> data){
        return getDistinctDays(data).stream().count();
    }

    public static HashMap<String,Integer> countActivity(List<MonitoredData> data){
        HashMap<String,Integer> result = new HashMap<>();
        data.stream().forEach(r-> {

            int count = (int) data.stream().map(x -> x.getActivities()).collect(Collectors.toList()).stream().filter(x -> x.equals(r.getActivities())).count();
            result.put(r.getActivities(),count);
                }
        );
        return result;
    }

    public static HashMap<String,Integer> countActivityByDay(List<MonitoredData> data,String day){
        HashMap<String,Integer> result = new HashMap<>();
        data.stream().forEach(r-> {

                    int count = (int) data.stream().filter(x->x.getStartDay().equals(day)||x.getEndDay().equals(day)).map(x -> x.getActivities()).collect(Collectors.toList()).stream().filter(x -> x.equals(r.getActivities())).count();
                    result.put(r.getActivities(),count);
                }
        );
        return result;
    }

    public static HashMap<Integer,HashMap<String,Integer>> dayActivities(List<MonitoredData>data){
        HashMap<Integer,HashMap<String,Integer>> result = new HashMap<>();
        List<String> distinctDays = getDistinctDays(data);
        distinctDays.stream().forEach(res->{
            result.put(distinctDays.indexOf(res),countActivityByDay(data,res));
        });
        return result;
    }

    public static HashMap<String,Date> getActivitiesDuration(List<MonitoredData> data){
       HashMap<String,Date> result = new HashMap<>();
       data.stream().forEach(elem->{
           result.put(elem.getActivities(),new Date(data.stream().filter(x->x.getActivities().equals(elem.getActivities())).map(x->x.getDateEnd().getTime()-x.getDateStart().getTime()).mapToLong(x->x.longValue()).sum()));});
       return result;
    }

    public static List<String> filterActivities(List<MonitoredData> data){
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");

            return countActivity(data).entrySet().stream().filter(
                activ-> data.stream().filter(x->activ.getKey().equals(x.getActivities())).map(x -> x.getDateEnd().getTime() - x.getDateStart().getTime()).mapToLong(x -> x.longValue()).filter(x ->
                    x<=300000).count()/((float)activ.getValue())>=0.9
            ).map(x->x.getKey()).collect(Collectors.toList());

    }

    public static List<String> greater(List<MonitoredData> data){
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");

        return countActivity(data).entrySet().stream().filter(
                activ-> data.stream().filter(x->activ.getKey().equals(x.getActivities())).map(x -> x.getDateEnd().getTime() - x.getDateStart().getTime()).mapToLong(x -> x.longValue()).sum()>=36000000
        ).map(x->x.getKey()).collect(Collectors.toList());


    }
}
